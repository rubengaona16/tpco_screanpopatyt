﻿using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tpco_ScreenPopATyT.Data;
using Tpco_ScreenPopATyT.Services;

namespace Tpco_ScreenPopATyT
{
    public partial class TEST : System.Web.UI.Page
    {
        private string Ucid = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                lblNotification.ForeColor = Color.Blue;
                ChkDocument.Enabled = false;
                ChkClosed.Enabled = false;
                clsGlobal.Log.RegisterEvent(BLL.Services.FEventType.Info, "Inicio", null, null, Environment.UserName);
                //string usuario = Sanitizer.GetSafeHtmlFragment(Request["usuario"]);
                //usuario = HttpUtility.UrlDecode(usuario);

                //string vdn = Sanitizer.GetSafeHtmlFragment(Request["vdn"]);
                //vdn = HttpUtility.UrlDecode(vdn);

                //string ani = Sanitizer.GetSafeHtmlFragment(Request["ani"]);
                //ani = HttpUtility.UrlDecode(ani);
                //Ucid = "00001129121641577795";


                Ucid = Sanitizer.GetSafeHtmlFragment(Request["ucid"]);
                Ucid = HttpUtility.UrlDecode(Ucid);
                Session["Ucid"] = Ucid;

                //string uui = Sanitizer.GetSafeHtmlFragment(Request["uui"]);
                //uui = HttpUtility.UrlDecode(uui);


            }
        }

        protected void AlertEnd(int value)
        {
            if(value==1)
                ClientScript.RegisterStartupScript(this.GetType(), "ramdom", "alert(1)", true);
            else if(value == 2)
                ClientScript.RegisterStartupScript(this.GetType(), "ramdom", "alert(2)", true);
            else if (value == 3)
                ClientScript.RegisterStartupScript(this.GetType(), "ramdom", "alert(3)", true);
        }


        protected void AlertAcc(object sender, EventArgs e)
        {
            var dsn = System.Configuration.ConfigurationManager.AppSettings["dsn"].ToString();
            var ucidSession = Session["Ucid"].ToString();
            clsGlobal.Log.RegisterEvent(BLL.Services.FEventType.Info, "botones", null, null, Environment.UserName);
            var Message = "";
            try
            {

                if (ChkAcc.Checked && !ChkDocument.Enabled && !ChkClosed.Enabled)
                {
                    var date = DateTime.Now;
                    //Message = "sweetAlert('Recuerda','" + ConfigurationManager.AppSettings["Message1"] + "','warning');";
                    ChkAcc.Enabled = false;
                    ChkDocument.Enabled = true;
                    ChkClosed.Enabled = false;
                    //ScriptManager.RegisterStartupScript(this, typeof(Page), "Message", Message, true);
                    AlertEnd(1);
                    var valuedate = GetDateButton(ucidSession, dsn);

                    if (!String.IsNullOrEmpty(valuedate))
                    {
                        var result = ResultMinutes(valuedate, date);
                        bd_hour1.InnerText = result;
                        if (!String.IsNullOrEmpty(result))
                        {
                            var insertDB = SetDateButton(result, dsn, 1, ucidSession);

                        }
                    }
                }



                if (ChkAcc.Checked && ChkDocument.Checked && !ChkClosed.Enabled)
                {
                    var date = DateTime.Now;
                    //Message = "sweetAlert('Recuerda','" + ConfigurationManager.AppSettings["Message2"] + "','warning');";
                    ChkAcc.Enabled = false;
                    ChkDocument.Enabled = false;
                    ChkClosed.Enabled = true;
                    //ScriptManager.RegisterStartupScript(this, typeof(Page), "Message", Message, true);
                    AlertEnd(2);
                    var valuedate = GetDateButton(ucidSession, dsn);

                    if (!String.IsNullOrEmpty(valuedate))
                    {
                        var result = ResultMinutes(valuedate, date);
                        bd_hour2.InnerText = result;
                        if (!String.IsNullOrEmpty(result))
                        {
                            var insertDB = SetDateButton(result, dsn, 2, ucidSession);
                        }
                    }

                }
                if (ChkAcc.Checked && ChkDocument.Checked && ChkClosed.Checked)
                {
                    var date = DateTime.Now;
                    ChkAcc.Enabled = false;
                    ChkDocument.Enabled = false;
                    ChkClosed.Enabled = false;
                    AlertEnd(3);
                    var valuedate = GetDateButton(ucidSession, dsn);

                    if (!String.IsNullOrEmpty(valuedate))
                    {
                        var result = ResultMinutes(valuedate, date);
                        bd_hour3.InnerText = result;
                        if (!String.IsNullOrEmpty(result))
                        {
                            var insertDB = SetDateButton(result, dsn, 3, ucidSession);
                            if (insertDB)
                                Session.Remove("Ucid");
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                Message = "sweetAlert('OPS','" + ConfigurationManager.AppSettings["MessageError"] + "','warning');";
            }

        }

        public string GetDateButton(string UCID, string Conexion)
        {
            var dtmFechaIni = "";
            var objDB = new DatabaseManager();
            var opened = false;
            var SQL = "GetData";
            Int64 rows;
            System.Data.SqlClient.SqlParameter[] arrayParameter = new System.Data.SqlClient.SqlParameter[1];

            try
            {
                if (objDB.OpenDatabase(Conexion))
                {
                    opened = true;
                    SqlParameter parameter1 = new SqlParameter();
                    parameter1.Direction = ParameterDirection.Input;
                    parameter1.ParameterName = "@UCID";
                    parameter1.SqlDbType = SqlDbType.VarChar;
                    parameter1.Value = UCID;
                    arrayParameter[0] = parameter1;
                    rows = objDB.QueryDatabase(SQL, ref arrayParameter);
                    objDB.CloseDatabase();
                    if (rows > 0)
                    {
                        var date = objDB.GetDataSet();
                        for (int i = 0; i < rows; i++)
                        {
                            dtmFechaIni = Convert.ToString(objDB.DataResults(i, "dtmFechaIni"));
                        }
                    }
                }
                return dtmFechaIni;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public string ResultMinutes(string InitialCall, DateTime date)
        {
            var ConvertResult = "";
            try
            {
                var ConvertDate = Convert.ToDateTime(InitialCall);
                var resut = date - ConvertDate;
                ConvertResult = resut.ToString();
                var valueString = ConvertResult.Substring(0, 8);
                return valueString;
            }

            catch (Exception ex)
            {
                return ConvertResult;
            }
        }

        public bool SetDateButton(string hour, string Conexion, int button, string UCID)
        {

            var objDB = new DatabaseManager();
            var opened = false;
            List<SqlParameter> lsParameter = new List<SqlParameter>();
            var SQL = "spInsertDataSPOP";
            Int64 rows;

            try
            {
                if (objDB.OpenDatabase(Conexion))
                {
                    opened = true;
                    SqlParameter parameter1 = new SqlParameter();
                    parameter1.Direction = ParameterDirection.Input;
                    parameter1.ParameterName = "@hour";
                    parameter1.SqlDbType = SqlDbType.VarChar;
                    parameter1.Value = hour;

                    SqlParameter parameter2 = new SqlParameter();
                    parameter2.Direction = ParameterDirection.Input;
                    parameter2.ParameterName = "@button";
                    parameter2.SqlDbType = SqlDbType.Int;
                    parameter2.Value = button;

                    SqlParameter parameter3 = new SqlParameter();
                    parameter3.Direction = ParameterDirection.Input;
                    parameter3.ParameterName = "@UCID";
                    parameter3.SqlDbType = SqlDbType.VarChar;
                    parameter3.Value = UCID;

                    lsParameter.Add(parameter1);
                    lsParameter.Add(parameter2);
                    lsParameter.Add(parameter3);

                    System.Data.SqlClient.SqlParameter[] arrayParameter = lsParameter.ToArray();
                    rows = objDB.QueryDatabase(SQL, ref arrayParameter);
                    if (rows >= 1)
                    {
                        var Test = Convert.ToInt32(objDB.DataResults(0, "R"));
                        if ((Convert.ToInt32(objDB.DataResults(0, "R"))) >= 0)
                        {

                            objDB.CloseDatabase();
                            opened = false;
                            return true;
                        }
                        else
                        {
                            switch (Convert.ToInt32(objDB.DataResults(0, "R")))
                            {
                                case -1:

                                    return false;
                                case -2:

                                    return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}