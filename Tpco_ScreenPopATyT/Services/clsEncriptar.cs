﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Security.Principal;
using Tpco_ScreenPopATyT.Entities;
//using InternalBBVA.Entities;

namespace Tpco_ScreenPopATyT.Data
{
    public class clsEncriptar
    {
        //Función para encriptar el login
        //falta reemplazar por sha1
        public static string mEncriptar(string A)
        {
            int I = 0;
            long N = 0;
            string h = null;
            for (I = 1; I <= Strings.Len(A); I++)
            {
                N = N + Strings.Asc(Strings.Mid(A, I, 1));
                N = (N * 1717 + 1717) % 1048576;
            }
            for (I = 1; I <= 7; I++)
            {
                N = (N * 997 + 997) % 1048576;
            }
            h = Strings.Right("0000" + Conversion.Hex(N), 4);
            for (I = 1; I <= Strings.Len(A); I++)
            {
                N = N + Strings.Asc(Strings.Mid(A, I, 1));
                N = (N * 997 + 997) % 1048576;
            }
            for (I = 1; I <= 7; I++)
            {
                N = (N * 1717 + 1717) % 1048576;
            }
            return h + Strings.Right("0000" + Conversion.Hex(N), 4);
        }

        /// <summary>
        /// Algoritmo de encriptación MD5
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string GetMD5(string text)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(text));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

    }

    public class clsEncriptaDesEncripta
    {

        private static TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
        private static byte[] Key
        {
            //24 bytes para 3des
            //------------------------------123456789012345678901234-----
            get { return Encoding.UTF8.GetBytes("7sw.;&+|/(q=120*.Z<>+'|="); }
        }

        private static byte[] Vector
        {
            //24 bytes para 3des 
            //------------------------------123456789012345678901234-----
            get { return Encoding.UTF8.GetBytes("#23(.*;|*'<Y>Ooa/(?+=$%a"); }
        }

        public static void Encrypt(byte[] Text, ref byte[] salida)
        {
            Transform(Text, des.CreateEncryptor(Key, Vector), ref salida);
        }

        public static void Decrypt(byte[] encryptedText, ref byte[] salida)
        {
            Transform(encryptedText, des.CreateDecryptor(Key, Vector), ref salida);
        }

        private static void Transform(byte[] Text, ICryptoTransform CryptoTransform, ref byte[] salida)
        {
            MemoryStream stream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(stream, CryptoTransform, CryptoStreamMode.Write);
            cryptoStream.Write(Text, 0, Text.Length);
            cryptoStream.FlushFinalBlock();
            stream.Close();
            salida = stream.ToArray();
        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = null;
            hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }

        public static byte[] StringToByteArray(string hex)
        {
            int NumberChars = 0;
            int intI = 0;
            byte[] bytes = null;
            NumberChars = hex.Length;
            bytes = new byte[(NumberChars / 2)];
            for (intI = 1; intI <= NumberChars / 2; intI++)
            {
                bytes[intI - 1] = Convert.ToByte(Strings.Mid(hex, intI * 2 - 1, 2), 16);
            }
            return bytes;
        }
    }

    public class RSACrypto
    {
        /// <summary>
        /// Método para generar claves RSA de 1024 bites para la encriptación y desencriptación de datos del lado del screenPop
        /// </summary>
        /// <returns></returns>
        public  static Keys GenerateKeys()
        {
            var rsa = new RSACryptoServiceProvider();
            var Keys = new Keys()
            {
                PublicKey = rsa.ToXmlString(false),
                PrivateKey = rsa.ToXmlString(true)
            };

            return Keys;
        }

        public static string Decrypt(string privateKey,string data)
        {
            var rsa = new RSACryptoServiceProvider();
            UTF8Encoding _encoder = new UTF8Encoding();
            byte[] dataByte;
            byte[] decryptedByte;
            var sb = string.Empty;
            int MaxLengthDES = 200;
            rsa.FromXmlString(privateKey);
            if (data.Length > MaxLengthDES)
            {
                var datos = data.Split(Convert.ToChar(","));
                for (int x = 0; x < datos.Length - 1; x++)
                {
                    dataByte = Convert.FromBase64String(datos[x]);
                    decryptedByte = rsa.Decrypt(dataByte, false);
                    sb += _encoder.GetString(decryptedByte);
                }
            }
            else
            {
                dataByte = Convert.FromBase64String(data);
                decryptedByte = rsa.Decrypt(dataByte, false);
                sb += _encoder.GetString(decryptedByte);
            }
            return sb;
        }

        public static string Encrypt(string privateKey, string data)
        {
            var sb = string.Empty;
            int MaxLength = 100;

            UTF8Encoding _encoder = new UTF8Encoding();

            try
            {
                var rsa = new RSACryptoServiceProvider();

                rsa.FromXmlString(privateKey);
                byte[] encryptedByteArray;
                byte[] dataToEncrypt;
                if (data.Length > MaxLength)
                {
                    var length = ((Convert.ToInt16(data.Length)) / MaxLength);
                    string[] datos = new string[length + 1];
                    for (int x = 0; x < length + 1; x++)
                    {

                        datos[x] = data.Substring((x) * MaxLength, ((x) == length ? data.Length - MaxLength * length : MaxLength ));
                        dataToEncrypt = _encoder.GetBytes(datos[x]);
                        encryptedByteArray = rsa.Encrypt(dataToEncrypt, false);
 
                        sb += Convert.ToBase64String(encryptedByteArray) + ",";
                    }
                }
                else
                {
                    dataToEncrypt = _encoder.GetBytes(data);
                    encryptedByteArray = rsa.Encrypt(dataToEncrypt, false);
                    String base64=Convert.ToBase64String(encryptedByteArray);
                    sb = base64;
                }
                  
            }
            catch (Exception e)
            {
                
            }
            return sb;
        }

        private RSACryptoServiceProvider _sp;
        public RSAParameters ExportParameters(bool includePrivateParameters)
        {
            return _sp.ExportParameters(includePrivateParameters);
        }

        public void InitCrypto(string keyFileName)
        {
            CspParameters cspParams = new CspParameters();
            cspParams.Flags = CspProviderFlags.UseMachineKeyStore;
            //'To avoid repeated costly key pair generation
            _sp = new RSACryptoServiceProvider(cspParams);
            //Dim path As String = keyFileName
            //Dim reader As System.IO.StreamReader = New StreamReader(path)
            //Dim data As String = reader.ReadToEnd()
            //_sp.FromXmlString(Data)

            _sp.FromXmlString(keyFileName);
        }

        public byte[] Encrypt(string txt)
        {
            byte[] result = null;
            ASCIIEncoding enc = new ASCIIEncoding();
            int numOfChars = enc.GetByteCount(txt);
            byte[] tempArray = enc.GetBytes(txt);
            result = _sp.Encrypt(tempArray, false);

            return result;
        }

        public byte[] Decrypt(byte[] txt)
        {
            byte[] result = null;
            try
            {
                _sp = new RSACryptoServiceProvider();
                result = _sp.Decrypt(txt, false);
            }
            catch (Exception ex)
            {
                result = txt;
            }

            return result;
        }
    }

    public class StringHelper
    {

        public static byte[] HexStringToBytes(string hex)
        {
            if (hex.Length == 0)
            {
                return new byte[] { 0 };
            }
            if (hex.Length % 2 == 1)
            {
                hex = "0" + hex;
            }
            byte[] result = new byte[hex.Length / 2];

            for (int i = 0; i <= hex.Length / 2 - 1; i++)
            {
                result[i] = byte.Parse(hex.Substring(2 * i, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
            }

            return result;
        }

        public static string BytesToHexString(byte[] input)
        {
            StringBuilder hexString = new StringBuilder(64);

            for (int i = 0; i <= input.Length - 1; i++)
            {
                hexString.Append(String.Format("{0:X2}", input[i]));
            }
            return hexString.ToString();
        }

        public static string BytesToDecString(byte[] input)
        {
            StringBuilder decString = new StringBuilder(64);
            for (int i = 0; i <= input.Length - 1; i++)
            {
                decString.Append(string.Format(i == 0 ? "{0:D3}" : "-{0:D3}", input[i]));                
            }
            return decString.ToString();
        }

        // Bytes are string
        public static string ASCIIBytesToString(byte[] input)
        {
            System.Text.ASCIIEncoding enc = new ASCIIEncoding();
            return enc.GetString(input);
        }
        public static string UTF16BytesToString(byte[] input)
        {
            System.Text.UnicodeEncoding enc = new UnicodeEncoding();
            return enc.GetString(input);
        }
        public static string UTF8BytesToString(byte[] input)
        {
            System.Text.UTF8Encoding enc = new UTF8Encoding();
            return enc.GetString(input);
        }

        // Base64
        public static string ToBase64(byte[] input)
        {
            return Convert.ToBase64String(input);
        }

        public static byte[] FromBase64(string base64)
        {
            return Convert.FromBase64String(base64);
        }
    }
}
