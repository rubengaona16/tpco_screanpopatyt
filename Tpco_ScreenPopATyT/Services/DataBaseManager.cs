﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Text;

namespace Tpco_ScreenPopATyT.Data
{
    /// <summary>
    /// Class to access databases.
    /// Must be the single entry point to database access
    /// </summary>
    /// <remarks></remarks>
    public class DatabaseManager
    {


        //Public Variables
        public string LastError { get; set; }

        //private variables
        private SqlConnection cn = new SqlConnection();
        private bool isOpened;
        private DataSet dsData = new DataSet();
        private object[,] myds;

        private string[] myColumnIndex;
        /// <summary>
        /// This constructor  set the  working mode based on configuration value.
        /// Default value of working mode is Normal
        /// </summary>
        /// <remarks></remarks>
        public DatabaseManager()
        {
            LastError = "";
            isOpened = false;
        }

        /// <summary>
        /// Public function to Open Database Connection
        /// </summary>
        /// <param name="ConnectionString">Connection String</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool OpenDatabase(string ConnectionString)
        {
            bool functionReturnValue = false;
            byte[] datos = null;
            datos = new byte[1];
            LastError = "";
            functionReturnValue = false;
            if (isOpened)
            {
                return functionReturnValue;
                //AlarmManager.Log("Previously opened database", AlarmManager.AlarmType.Fatal)
            }
            if (string.IsNullOrEmpty(ConnectionString))
            {
                return functionReturnValue;
                //AlarmManager.Log("Invalid connection string", AlarmManager.AlarmType.Security)
            }
            try
            {
                clsEncriptaDesEncripta.Decrypt(clsEncriptaDesEncripta.StringToByteArray(ConnectionString), ref datos);
                ConnectionString = Encoding.UTF8.GetString(datos);
                cn.ConnectionString = ConnectionString;
                cn.Open();
                functionReturnValue = true;
                isOpened = true;
                //AlarmManager.Log("Database opened", AlarmManager.AlarmType.Informative)
            }
            catch (Exception ex)
            {
                LastError = ex.Message;
                //AlarmManager.Log("Error opening database:" + ex.Message, AlarmManager.AlarmType.Security)
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Public function to close an opened database
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool CloseDatabase()
        {
            bool functionReturnValue = false;
            LastError = "";
            functionReturnValue = false;
            if (!isOpened)
            {
                return functionReturnValue;
                //AlarmManager.Log("Not previously opened database", AlarmManager.AlarmType.Fatal)
            }
            try
            {
                cn.Close();
                isOpened = false;
                functionReturnValue = true;
                //AlarmManager.Log("Database closed", AlarmManager.AlarmType.Informative)
            }
            catch (Exception ex)
            {
                LastError = ex.Message;
                // AlarmManager.Log("Error closing database:" + ex.Message, AlarmManager.AlarmType.Fatal)
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Public function to call an stored procedure that returns a result set of data.
        /// Timeout value is 5 minutes.
        /// If function succeds it returns the number of rows.
        /// If not data is returned the function return 0 value.
        /// Any error during this function return a -999 value.
        /// For debuging porpouses the time taken between function start and end is logged.
        /// Data is stored not only in a dataset but also in a matrix.
        /// After you use this method you can retrieve the returned data using DataResults function
        /// </summary>
        /// <param name="SQL">Stored procedure name</param>
        /// <param name="ArrayParameters">Array parameters of the stored procedure to be called</param>
        /// <returns></returns>
        /// <remarks>
        /// </remarks>
        public long QueryDatabase(string SQL, ref System.Data.SqlClient.SqlParameter[] ArrayParameters)
        {
            long functionReturnValue = 0;
            int i = 0;
            System.Data.SqlClient.SqlDataReader reader1 = null;
            bool read1 = false;
            System.Data.SqlClient.SqlCommand command1 = new System.Data.SqlClient.SqlCommand();
            LastError = "";
            command1.CommandTimeout = 5 * 60;
            functionReturnValue = -999;
            read1 = false;
            reader1 = null;
            if (!isOpened)
            {
                return functionReturnValue;
                //AlarmManager.Log("Not previously opened database", AlarmManager.AlarmType.Fatal)
            }
            else
            {
                try
                {
                    dsData.Clear();
                    dsData.Tables.Clear();
                    dsData.Tables.Add(new DataTable());
                    dsData.EnforceConstraints = false;
                    command1.Connection = cn;
                    //input
                    if ((ArrayParameters != null))
                    {
                        command1.Parameters.AddRange(ArrayParameters);
                    }
                    command1.CommandText = SQL;
                    command1.CommandType = CommandType.StoredProcedure;
                    reader1 = command1.ExecuteReader();
                    read1 = true;
                    dsData.Tables[0].Load(reader1);
                    //output
                    if ((ArrayParameters != null))
                    {
                        for (i = 1; i <= ArrayParameters.Length; i++)
                        {
                            System.Data.SqlClient.SqlParameter p = new System.Data.SqlClient.SqlParameter();
                            p = command1.Parameters[i - 1];
                            ArrayParameters[i - 1] = p;
                        }
                    }
                    //clean
                    command1.Parameters.Clear();
                    command1 = null;
                    ConvertToMyds();
                    //Remember that if you have set variable nocount=on 
                    //it does not return the affected rows number
                    functionReturnValue = dsData.Tables[0].Rows.Count;
                }
                catch (Exception ex)
                {
                    functionReturnValue = -999;
                    LastError = ex.Message;
                    // AlarmManager.Log("Error querying database:" + ex.Message, AlarmManager.AlarmType.Fatal)
                    try
                    {
                        command1.Parameters.Clear();
                        command1 = null;

                    }
                    catch (Exception ex2)
                    {
                    }
                }
            }
            if (read1)
            {
                reader1.Close();
                read1 = false;
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Public function To Execute stored procedures that not return any result set.
        /// Timeout value is 5 minutes.
        /// If function succeds it returns the number of rows.
        /// If not data is returned the function return 0 value.
        /// Any error during this function return a -999 value.
        /// For debuging porpouses the time taken between function start and end is logged.
        /// Data is stored not only in a dataset but also in a matrix.
        /// </summary>
        /// <param name="SQL">Stored procedure name</param>
        /// <param name="ArrayParameters">Array parameters of the stored procedure to be called</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public long ExecuteNonQuery(string SQL, ref System.Data.SqlClient.SqlParameter[] ArrayParameters)
        {
            long functionReturnValue = 0;
            int i = 0;
            System.Data.SqlClient.SqlDataReader reader1 = null;
            bool read1 = false;
            System.Data.SqlClient.SqlCommand command1 = new System.Data.SqlClient.SqlCommand();
            LastError = "";
            functionReturnValue = -999;
            read1 = false;
            command1.CommandTimeout = 5 * 60;
            reader1 = null;
            if (!isOpened)
            {
                return functionReturnValue;
                //AlarmManager.Log("Not previously opened database", AlarmManager.AlarmType.Fatal)
            }
            else
            {
                try
                {
                    command1.Connection = cn;
                    //input
                    if ((ArrayParameters != null))
                    {
                        command1.Parameters.AddRange(ArrayParameters);
                    }
                    command1.CommandText = SQL;
                    command1.CommandType = CommandType.StoredProcedure;
                    //Remember that if you have set variable nocount=on 
                    //it does not return the affected rows number
                    functionReturnValue = command1.ExecuteNonQuery();
                    if (functionReturnValue == -1)
                    {
                        functionReturnValue = 0;
                    }
                    //output
                    if ((ArrayParameters != null))
                    {
                        for (i = 1; i <= ArrayParameters.Length; i++)
                        {
                            System.Data.SqlClient.SqlParameter p = new System.Data.SqlClient.SqlParameter();
                            p = command1.Parameters[i - 1];
                            ArrayParameters[i - 1] = p;
                        }
                    }
                    //clean
                    command1.Parameters.Clear();
                    command1 = null;
                }
                catch (Exception ex)
                {
                    functionReturnValue = -999;
                    LastError = ex.Message;
                    //AlarmManager.Log("Error executing nonquery database:" + ex.Message, AlarmManager.AlarmType.Fatal)
                    try
                    {
                        command1.Parameters.Clear();
                        command1 = null;

                    }
                    catch (Exception ex2)
                    {
                    }
                }
            }
            if (read1)
            {
                reader1.Close();
                read1 = false;
            }
            return functionReturnValue;
        }


        /// <summary>
        /// Public method to get dataset of a previous QueryDatabase.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public DataSet GetDataSet()
        {
            //AlarmManager.Log("GetDataSet ", AlarmManager.AlarmType.Informative)
            return dsData;
        }

        /// <summary>
        /// Private Method to convert dataset into a matrix.
        /// If any value returned from database is null it is assigned a nothing value.
        /// It also populates the internal myColumnIndex HashTable
        /// </summary>
        /// <remarks></remarks>
        private void ConvertToMyds()
        {
            int i = 0;
            int j = 0;
            DataRow myRow = null;
            myds = new object[dsData.Tables[0].Rows.Count, dsData.Tables[0].Columns.Count];
            for (i = 0; i <= dsData.Tables[0].Rows.Count - 1; i++)
            {
                myRow = dsData.Tables[0].Rows[i];
                for (j = 0; j <= dsData.Tables[0].Columns.Count - 1; j++)
                {
                    if (Information.IsDBNull(myRow[j]))
                    {
                        myds[i, j] = null;
                    }
                    else
                    {
                        myds[i, j] = myRow[j];
                    }
                }
            }
            myColumnIndex = new string[dsData.Tables[0].Columns.Count];
            for (j = 0; j <= dsData.Tables[0].Columns.Count - 1; j++)
            {
                myColumnIndex[j] = dsData.Tables[0].Columns[j].ColumnName.ToUpper().Trim();
            }
        }

        /// <summary>
        /// Public Function to get row, column values of results of QueryDatabase.
        /// Row and Column  are zero index.
        /// Column can also be Field Name
        /// </summary>
        /// <param name="Row">Row (starting from 0)</param>
        /// <param name="Col">Column (starting from 0) can also be Field Name</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public object DataResults(long Row, object Col)
        {
            object functionReturnValue = null;
            int j = 0;
            int i = 0;
            bool hasFound = false;
            if (Information.IsNumeric(Col))
            {
                j = Convert.ToInt32(Col);
                if (j > Information.UBound(myColumnIndex, 1))
                {
                    //AlarmManager.Log("DataResults:invalid column index", AlarmManager.AlarmType.Fatal)
                    throw new Exception("invalid column index");
                    return functionReturnValue;
                }
            }
            else
            {
                hasFound = false;
                for (i = 0; i <= Information.UBound(myColumnIndex, 1); i++)
                {
                    if (myColumnIndex[i] == Col.ToString().ToUpper().Trim())
                    {
                        j = i;
                        hasFound = true;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                if (!hasFound)
                {
                    // AlarmManager.Log("DataResults:invalid column index", AlarmManager.AlarmType.Fatal)
                    throw new Exception("invalid column index");
                    return functionReturnValue;
                }
            }
            if (Row > Information.UBound(myds, 1))
            {
                // AlarmManager.Log("DataResults:invalid row index", AlarmManager.AlarmType.Fatal)
                throw new Exception("invalid row index");
                return functionReturnValue;
            }
            functionReturnValue = myds[Row, j];
            return functionReturnValue;
        }
    }
}
