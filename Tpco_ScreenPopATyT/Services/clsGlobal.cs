﻿

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
//using Teleperformance.Helpers.Logger;
using BLL.Services;
using System.IO;
using Tpco_ScreenPopATyT.WSTPString;

namespace Tpco_ScreenPopATyT.Services
{
    public class clsGlobal
    {
        static string fdns;
        static string fCallDataService;
        static string fIp;
        static string fUrlCallDataService;


        public static clsLog Log;
        
        public static string dns
        {
            get { return fdns; }
        }

        public static string CallDataService
        {
            get { return fCallDataService; }
        }

        public static string UrlCallDataService
        {
            get { return fUrlCallDataService; }
        }

        public static string Ip
        {
            get { return fIp; }
        }

       


        public static string AppFullPath()
        {
            return System.AppDomain.CurrentDomain.BaseDirectory;
        }

        public static void DeleteFilesFolderByDate(string DirectoryPath, int DaysToDel)
        {
            DirectoryInfo Source = new DirectoryInfo(DirectoryPath);
            if (Source.Exists)
            {
                foreach (FileInfo FI in Source.GetFiles())
                {
                    DateTime CreationTime = FI.CreationTime;
                    DateTime DeleteTime = DateTime.Now - new TimeSpan(DaysToDel, 0, 0, 0);
                    if (FI.CreationTime < DeleteTime)
                    {
                        FI.Delete();
                    }
                }
            }
        }

        public static void Initialize()
        {
            const string LogDirName = "Log";
            const string LogExt = ".Log";
            const int DaysAgo = 30;

            if (!Directory.Exists(AppFullPath() + Path.DirectorySeparatorChar + LogDirName))
            {
                Directory.CreateDirectory(AppFullPath() + Path.DirectorySeparatorChar + LogDirName);
            }

            Log = new clsLog(AppFullPath() + LogDirName + Path.DirectorySeparatorChar + Guid.NewGuid().ToString() + LogExt);
            Log.RegisterEvent(BLL.Services.FEventType.Info, "Start Application.", null, null, Environment.UserName);
            DeleteFilesFolderByDate(AppFullPath() + Path.DirectorySeparatorChar + LogDirName, DaysAgo);

            string TPStrings = ConfigurationManager.AppSettings["TPStrings"];
            string TPStringsBK = ConfigurationManager.AppSettings["TPStringsBK"];
            string TPStringsUser = ConfigurationManager.AppSettings["TPStringsUser"];
            string TPStringsPassword = ConfigurationManager.AppSettings["TPStringsPassword"];
            string ApplicationId = ConfigurationManager.AppSettings["ApplicationId"];
            string ApplicationName = ConfigurationManager.AppSettings["ApplicationName"];
            fCallDataService= ConfigurationManager.AppSettings["CallDataService"];
            fUrlCallDataService = ConfigurationManager.AppSettings["UrlCallDataService"];
          


            wsTPStrings1 TPStringsService = new wsTPStrings1();
            TPStringsService.Url = TPStrings;
            ResponseGetParamsByIdloginList TPStringsResponse = new ResponseGetParamsByIdloginList();
            string ipAddress = GetLocalIPAddress();

            try
            {
                TPStringsResponse = TPStringsService.GetParamsByIdloginList(Convert.ToInt32(ApplicationId), TPStringsUser, TPStringsPassword, ApplicationName, Environment.UserName, TPStringsUser, ipAddress);
            }
            catch (Exception ex)
            {
                TPStringsService.Url = TPStringsBK;
                TPStringsResponse = TPStringsService.GetParamsByIdloginList(Convert.ToInt32(ApplicationId), TPStringsUser, TPStringsPassword, ApplicationName, Environment.UserName, TPStringsUser, ipAddress);
                //clsUtils.Log(LogLevel.Fatal, "Error consumiendo TPTrings:" + ex.Message);
            }

            if (TPStringsResponse.Parametros != null)
            {
                if (TPStringsResponse.Parametros.Length > 0)
                {
                    for (int I = 0; I <= TPStringsResponse.Parametros.Length - 1; I++)
                    {
                        if (TPStringsResponse.Parametros[I].Parametro == "DSN")
                        {
                            fdns = TPStringsResponse.Parametros[I].Valor;
                        }
                    }
                }
            }

            fIp = GetLocalIPAddress();
        }



        private static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}