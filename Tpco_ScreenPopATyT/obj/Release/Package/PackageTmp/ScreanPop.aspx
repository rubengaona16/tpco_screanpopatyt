﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScreanPop.aspx.cs" Inherits="Tpco_ScreenPopATyT.App.ScreanPop" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui.css" rel="stylesheet" />

    <script src="Scripts/jquery-3.4.1.min.js" type="text/javascript"></script>
    <%--<script src="../Scripts/jquery-ui.js" type="text/javascript"></script>--%>
    <script src="Scripts/bootstrap.min.js" type="text/javascript"></script>

   
    <!-- sweetalert -->
    <script src="Scripts/sweetalert.min.js"></script>




    <title>ScreanPopTyT</title>
    <script>    
        function alert() {           
            swal({
                title: "Alerta",
                text: "ES MANDATORIO DOCUMENTAR LAS INTERACCIONES Y GUARDARLAS AL FINAL DE LA LLAMADA PARA CONTINUAR, EL NO GUARDARLA ES UN INCLUMPLIMIENTO GRAVE ALOS LINEAMIENTOS DE SEGURIDAD Y PRIVACIDAD DE LA CAMPAÑA",
                icon: "warning",
                buttons:                  
                    'OK'
                ,
                dangerMode: true,
            }).then(function (isConfirm) {
                if (isConfirm) {
                    swal({
                        position: 'top-end',                        
                        icon: 'success',
                        title: 'Check list done! Thanks for following the process',
                        showConfirmButton: false,
                        timer: 3000
                    }).then(function () {
                        console.log('done')
                        form.submit(); // <--- submit form programmatically
                    });
                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            })


        }
    </script>



</head>


<body>

    <center>
        <nav class="navbar navbar-default">
            <div class="container-fluid">

               <%-- <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">--%>
                  
                    <br />
                    <asp:Label Text="Recuerda seguir el orden de los botones para finalizar el proceso" ID="lblNotification" runat="server"></asp:Label>

                    <form class="navbar-form" role="search" runat="server">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-2">
                                <%-- <div class="input-group">
                                    <asp:Button type="submit" class="btn btn-default" Text="KB-Articles" runat="server" />
                                </div>--%>

                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        KB-Articles
                                    <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="https://attone.lightning.force.com/lightning/r/Knowledge__kav/ka04M000000VoTgQAK/view" target="_blank">Autenticación CRM</a></li>
                                        <li><a href="https://attone.lightning.force.com/lightning/r/Knowledge__kav/ka04M000000ZCdeQAG/view" target="_blank">Autenticación Mobility</a></li>
                                        <li><a href="https://attone.lightning.force.com/lightning/r/Knowledge__kav/ka04M000000t0pwQAA/view" target="_blank">Autenticación otras herramientas</a></li>
                                        <li><a href="https://attone.lightning.force.com/lightning/r/Knowledge__kav/ka04M000000sgnWQAQ/view" target="_blank">Non-Customer Contact </a></li>
                                        <li><a href="https://attone.lightning.force.com/lightning/r/Knowledge__kav/ka04M000000Vl8VQAS/view" target="_blank">CBR Disclosure </a></li>
                                        <li><a href="https://attone.lightning.force.com/lightning/r/Knowledge__kav/ka04M000000VkFfQAK/view" target="_blank">Integrity Hub</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <asp:CheckBox runat="server" ID="ChkAcc" OnCheckedChanged="AlertAcc" AutoPostBack="true" />
                                    </span>
                                    <asp:Button type="submit" class="btn btn-default" Text="1.Acc Authentication" runat="server" />
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <asp:CheckBox runat="server" ID="ChkDocument" OnCheckedChanged="AlertAcc" AutoPostBack="true" />
                                    </span>
                                    <asp:Button type="submit" class="btn btn-default" Text="2.Document the Call" runat="server" />
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <asp:CheckBox runat="server" ID="ChkClosed" OnCheckedChanged="AlertAcc" AutoPostBack="true" />
                                    </span>
                                    <asp:Button type="submit" class="btn btn-default" Text="3.Closed Interaction" runat="server" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
           <%-- </div>--%>
        </nav>
    </center>
</body>
</html>


