﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SPOP.aspx.cs" Inherits="Tpco_ScreenPopATyT.TEST" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <%--    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />--%>

    <meta http-equiv="x-ua-compatible" content="IE=11" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />

    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui.css" rel="stylesheet" />

    <script src="Scripts/jquery-3.4.1.min.js" type="text/javascript"></script>
    <%--<script src="../Scripts/jquery-ui.js" type="text/javascript"></script>--%>
    <script src="Scripts/bootstrap.min.js" type="text/javascript"></script>


    <!-- sweetalert -->
    <script src="Scripts/sweetalert.min.js"></script>
    <title></title>

    <style>
        nav.navbar {
            background-color: #D5D7DC;
        }
    </style>

    <script>    
        function alert(value) {
            if (value == 1) {
                console.log(value);
                swal({
                    title: "Recuerda",
                    text: "RECUERDA SIEMPRE BUSCAR/AUTENTICAR TODAS LAS CUENTAS DE USUARIO EN LA LLAMADA",
                    icon: "warning",
                    buttons: 'OK',
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    allowEscapeKey: false,
                    dangerMode: false,
                })
            }
            else if (value == 2) {
                swal({
                    title: "Recuerda",
                    text: "RECUERDA SIEMPRE DEJAR LAS NOTAS DEL REQUERIMIENTO DEL CLIENTE Y LAS ACCIONES QUE TOMASTE, ADEMAS RECUERDA REALIZAR TUS VENTAS DE FORMA CORRECTA Y SOLO INCLUYEND LOS PRODUCTOS/SERVICIOS REQUERIDOS POR EL CLIENTE",
                    icon: "warning",
                    buttons: 'OK',
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    allowEscapeKey: false,
                    dangerMode: false,
                })
            }

            else if (value == 3) {
                swal({
                    title: "Alerta",
                    text: "ES MANDATORIO DOCUMENTAR LAS INTERACCIONES Y GUARDARLAS AL FINAL DE LA LLAMADA PARA CONTINUAR, EL NO GUARDARLA ES UN INCLUMPLIMIENTO GRAVE ALOS LINEAMIENTOS DE SEGURIDAD Y PRIVACIDAD DE LA CAMPAÑA",
                    icon: "warning",
                    buttons: 'OK',
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    allowEscapeKey: false,
                    dangerMode: false,
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Check list done! Thanks for following the process',
                            showConfirmButton: false,
                            timer: 3000
                        }).then(function () {
                            window.close();
                            // <--- submit form programmatically
                        });
                    } else {

                        swal("Cancelled", "Your imaginary file is safe :)", "error");
                    }
                })
            }
            


        }
    </script>
</head>
<body>
    <center>
        <nav class="navbar navbar-default" id="navPrincipal">
            <div class="container-fluid">
                <br />
                <asp:Label Text="Recuerda seguir el orden de los botones para finalizar el proceso" ID="lblNotification" runat="server"></asp:Label>
                <br />
                <br />
                <form class="navbar-form" role="search" runat="server">
                    <div class="row" runat="server">
                        <div class="col-lg-2"></div>

                        <div class="col-lg-2">
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" style="background-color:blue" type="button" id="Button1" runat="server" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    KB-Articles
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="https://attone.lightning.force.com/lightning/r/Knowledge__kav/ka04M000000VoTgQAK/view" target="_blank">Autenticación CRM</a></li>
                                    <li><a href="https://attone.lightning.force.com/lightning/r/Knowledge__kav/ka04M000000ZCdeQAG/view" target="_blank">Autenticación Mobility</a></li>
                                    <li><a href="https://attone.lightning.force.com/lightning/r/Knowledge__kav/ka04M000000t0pwQAA/view" target="_blank">Autenticación otras herramientas</a></li>
                                    <li><a href="https://attone.lightning.force.com/lightning/r/Knowledge__kav/ka04M000000sgnWQAQ/view" target="_blank">Non-Customer Contact </a></li>
                                    <li><a href="https://attone.lightning.force.com/lightning/r/Knowledge__kav/ka04M000000Vl8VQAS/view" target="_blank">CBR Disclosure </a></li>
                                    <li><a href="https://attone.lightning.force.com/lightning/r/Knowledge__kav/ka04M000000VkFfQAK/view" target="_blank">Integrity Hub</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon" id="sp_check1" runat="server">
                                    <asp:CheckBox runat="server" ID="ChkAcc" OnCheckedChanged="AlertAcc" AutoPostBack="true" />
                                </span>
                                <asp:Button type="submit" class="btn btn-primary" style="background-color:blue" Text="1.Acc Authentication" runat="server" />
                            </div>
                            <span class="badge" id="bd_hour1" runat="server"></span>
                        </div>

                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon" id="sp_check2" runat="server">
                                    <asp:CheckBox runat="server" ID="ChkDocument" OnCheckedChanged="AlertAcc" AutoPostBack="true" />
                                </span>
                                <asp:Button type="submit" class="btn btn-primary" style="background-color:blue" Text="2.Document the Call" runat="server" />
                            </div>
                            <span class="badge" id="bd_hour2" runat="server"></span>
                        </div>

                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon" id="sp_check3" runat="server">
                                    <asp:CheckBox runat="server" ID="ChkClosed" OnCheckedChanged="AlertAcc" AutoPostBack="true" />
                                </span>
                                <asp:Button type="submit" class="btn btn-primary" style="background-color:blue" Text="3.Closed Interaction" runat="server" />
                            </div>
                            <span class="badge" id="bd_hour3" runat="server"></span>
                        </div>
                    </div>
                </form>


            </div>
        </nav>
    </center>
</body>
</html>
