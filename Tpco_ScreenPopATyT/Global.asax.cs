﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace Tpco_ScreenPopATyT
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType.Tls | (System.Net.SecurityProtocolType.Tls11 | (System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Ssl3)));
            // Código que se ejecuta al iniciar la aplicación
            Services.clsGlobal.Initialize();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}